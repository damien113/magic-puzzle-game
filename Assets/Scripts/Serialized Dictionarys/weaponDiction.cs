﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class weaponDiction {
    public string weaponName;
    public string weaponDesc;
    public int weaponDamage;
    public float weaponSpeed;
    public Weapons weaponEnum;
    public Texture weaponIcon;
    public GameObject weaponPrefab;
}
