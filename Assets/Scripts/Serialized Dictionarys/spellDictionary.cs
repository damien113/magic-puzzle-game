﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class spellDictionary{
    public string spellName;
    public spells spell;
    public GameObject prefab;
    public List<spellElements> elements = new List<spellElements> { spellElements.NONE,spellElements.NONE,spellElements.NONE,spellElements.NONE };
    public SpellAnimations animation;
    public SpellTypes type;
}
