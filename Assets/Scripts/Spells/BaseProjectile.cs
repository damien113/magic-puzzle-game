﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseProjectile : MonoBehaviour {

    public GameObject owner;
    public float projectileSpeed;
    public int projectileDamage;

    private void LateUpdate()
    {
        transform.position = transform.position + transform.forward * projectileSpeed * Time.deltaTime;  
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other != null && owner != null)
        {
            if (owner.GetComponent<PlayerBase>())
            {
                other.gameObject.GetComponent<EnemyHealth>().Damage(owner.GetComponent<PlayerBase>(), projectileDamage);
            }
            else if (other != null && owner != null && owner.GetComponent<EnemyBase>())
            {
                other.gameObject.GetComponent<PlayerHealth>().Damage(owner.GetComponent<EnemyBase>(), projectileDamage);
                Destroy(gameObject);
            }
        }
    }

    void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
