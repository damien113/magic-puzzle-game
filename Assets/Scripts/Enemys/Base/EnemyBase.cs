﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(EnemyHealth))]
public class EnemyBase : MonoBehaviour
{
    private void Start()
    {
        SetupEvents();
    }

    void SetupEvents()
    {
        #region Enemy Health
        Messenger.AddListener<PlayerBase, int, EnemyBase>("enemy_damaged", EnemyDamagedEventHandler);
        Messenger.AddListener<EnemyBase, int, EnemyBase>("enemy_healed", EnemyHealedEventHandler);
        Messenger.AddListener<PlayerBase, EnemyBase>("enemy_killed", EnemyKilledEventHandler);
        #endregion
    }


    public void EnemyDamagedEventHandler(PlayerBase attacker, int damage_amount, EnemyBase target)
    {

    }

    public void EnemyHealedEventHandler(EnemyBase healer, int damage_amount, EnemyBase target)
    {

    }

    public void EnemyKilledEventHandler(PlayerBase killer, EnemyBase target)
    {

    }

}
