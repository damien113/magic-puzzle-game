﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    /// <summary>
    /// The maximum amount of health the enemy can have.
    /// </summary>
    public int maxHitPoints;

    /// <summary>
    /// The amount of health the enemy currently has.
    /// </summary>
    public int hitPoints;
    

    /// <summary>
    /// This function damages the player. It triggers a event "enemy_damaged"
    /// </summary>
    /// <param name="The gameobject of what attacked the player."></param>
    /// <param name="The amount of damage to apply."></param>
    public void Damage(PlayerBase attacker, int damage)
    {
        int tmp = hitPoints - damage;
        if (tmp > 0)
        {
            hitPoints = tmp;
            Messenger.Broadcast<PlayerBase, int, EnemyBase>("enemy_damaged", attacker, damage, GetComponent<EnemyBase>());
        }
        else
        {
            Kill(attacker);
        }
    }

    /// <summary>
    /// This function heals the player. It triggers a event "enemy_healed"
    /// </summary>
    /// <param name="The gameobject of what healed the player."></param>
    /// <param name="The amount of health to apply."></param>
    public void Heal(EnemyBase healer, int health)
    {
        int tmp = hitPoints + health;
        if (tmp < maxHitPoints)
        {
            hitPoints = tmp;
            Messenger.Broadcast<EnemyBase, int, EnemyBase>("enemy_healed", healer, health, GetComponent<EnemyBase>());
        }
        else
        {
            hitPoints = maxHitPoints;
        }
    }

    /// <summary>
    /// This is a functiont that kills the player. It broadcasts am essage that theplayer died called "enemy_killed"
    /// </summary>
    /// <param name="The gameobject who killed the player."></param>
    public void Kill(PlayerBase killer)
    {
        Destroy(gameObject);
        Messenger.Broadcast<PlayerBase, EnemyBase>("enemy_killed", killer, GetComponent<EnemyBase>());
    }

    /// <summary>
    /// This will set the health of the player to a value that is passed as a argument. This was added so it could be called when the
    /// vitality stat was upgraded on the players stats. A event  called "ply_maxhpset_event" is broadcasted at the end.
    /// </summary>
    /// <param name="The amountof health to set."></param>
    /// <param name="Should the current health is to be set?"></param>
    public void SetMaxHealth(int health, bool setCurrentHealth)
    {
        if (setCurrentHealth)
        {
            maxHitPoints = health;
            hitPoints = maxHitPoints;
        }
        else
        {
            maxHitPoints = health;
        }
    }

    /// <summary>
    /// This function allows you to manually set the current health of the enemy.
    /// </summary>
    /// <param name="healthToSet">Amount to set the health as</param>
    public void SetCurrentHealth(int healthToSet)
    {
        if (healthToSet >= 1)
        {
            hitPoints = healthToSet;
            Debug.Log("<color=orange>An enemys health has been manually set to '" + healthToSet + "'.</color>");
        }
        else
        {
            Debug.Log("<color=red>The number passed to SetCurrentHealth was below 1. It needs to be 1 or higher to set the health as that number.</color>");
        }
    }

}
