﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum SpellTypes
{
    PROJECTILE,
    BUFF,
    CURSE,
    HEALING
}

/// <summary>
/// Enums for the intergers to put into the blend tree for htier corresponding animations so that the spell dictionry can use it.
/// </summary>
public enum SpellAnimations
{
    ONE_HANDED_BUFF_SPELL,
    ONE_HANDED_HAND_THRUST_ATTACK,
    ONE_HANDED_FRISBEE_ATTACK,
    ONE_HANDED_UNDERHAND_ATTACK,
    TWO_HANDED_ARMS_IN_AIR_SPELL,
    TWO_HANDED_GROUND_POUND_AOE,
    TWO_HANDED_TPOSE_AOE,
    TWO_HANDED_PUSH_ATTACK,
    TWO_HANDED_BALL_ATTACK,
    TWO_HANDED_LONG_BEAM1_ATTACK,
    TWO_HANDED_LONG_BEAM2_ATTACK,
    TWO_HANDED_PULL_CLAP_ATTACK
}

/// <summary>
/// The different weapons that the player can use
/// </summary>
public enum Weapons
{
    FIRE_STAFF
}

/// <summary>
/// This enum is the types of damage that can occur to enemys or the player.
/// </summary>
public enum DamageTypes { MAGIC,PHYSICAL,WORLD};

/// <summary>
/// Names of the different stats and corresponding enums
/// </summary>
public enum Stats { STRENGTH, INTELLIGENCE, VITALITY, WISDOM, COUNT};

/// <summary>
/// Directions
/// </summary>
public enum Directions { LEFT, UP, RIGHT, DOWN };

/// <summary>
/// All the elements that can be attuned
/// </summary>
public enum spellElements { NONE, FIRE, EARTH, WATER, AIR };

/// <summary>
/// All the enums for the different spells.
/// </summary>
public enum spells
{
    NONE,
    AIR_BLAST,
    EARTH_BLAST,
    FIRE_BLAST, // Shoots a ball of fire as a damaging projectile. 
    WATER_BLAST,
}
public class EnumStore : MonoBehaviour {
}
