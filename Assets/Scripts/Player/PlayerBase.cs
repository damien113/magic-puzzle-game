﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(PlayerSpells))]
[RequireComponent(typeof(PlayerMelee))]
[RequireComponent(typeof(PlayerCamera))]
[RequireComponent(typeof(PlayerStats))]
[RequireComponent(typeof(PlayerControls))]
[RequireComponent(typeof(PlayerHealth))]
[RequireComponent(typeof(PlayerWeapons))]
[RequireComponent(typeof(PlayerAnimation))]

public class PlayerBase : MonoBehaviour {

    private void Start()
    {
        SetupEvents();
    }

    #region Events
    void SetupEvents()
    {
        #region Stats
        Messenger.AddListener<Stats, int, PlayerBase> ("stat_updated", StatUpdatedEvent);
        Messenger.AddListener<Stats, int, PlayerBase>("stat_addedpoints", StatAddedPoints);
        Messenger.AddListener<Stats, int, PlayerBase>("stat_removedpoints", StatRemovedPointsEvent);
        Messenger.AddListener<Stats, int, PlayerBase>("stat_set", StatSetEvent);
        Messenger.AddListener<PlayerBase>("stats_reset", StatResetEvent);
        #endregion

        #region Health
        Messenger.AddListener<EnemyBase,int,PlayerBase>("player_damaged", PlayerDamagedEvent);
        Messenger.AddListener<PlayerBase, int,PlayerBase>("player_healed", PlayerHealedEvent);
        Messenger.AddListener<EnemyBase,PlayerBase>("player_killed", PlayerKilledEvent);
        #endregion

        #region Spells
        Messenger.AddListener<spellElements, PlayerBase>("spells_element1_attuned", Element1AttunedEvent);
        Messenger.AddListener<spellElements, PlayerBase>("spells_element2_attuned", Element2AttunedEvent);
        Messenger.AddListener<spellElements, PlayerBase>("spells_element3_attuned", Element3AttunedEvent);
        Messenger.AddListener<spellElements, PlayerBase>("spells_element4_attuned", Element4AttunedEvent);
        Messenger.AddListener<spells, PlayerBase>("spells_invoked", SpellInvokedEvent);
        Messenger.AddListener<spells, PlayerBase>("spells_cast", SpellCastEvent);
        #endregion

        #region Weapons
        Messenger.AddListener<Weapons, PlayerBase>("weapon_equipped", WeaponEquippedEvent);
        Messenger.AddListener<Weapons, PlayerBase>("weapon_unequipped", WeaponUnequippedEvent);
        #endregion


    }

    #region Stats
    void StatUpdatedEvent(Stats stat, int points, PlayerBase player)
    {

    }
    void StatAddedPoints(Stats stat, int points, PlayerBase player)
    {

    }
    void StatRemovedPointsEvent(Stats stat, int points, PlayerBase player)
    {

    }
    void StatSetEvent(Stats stat, int points, PlayerBase player)
    {

    }
    void StatResetEvent(PlayerBase player)
    {

    }
    #endregion

    #region Health
    void PlayerDamagedEvent(EnemyBase attacker, int damage, PlayerBase player)
    {

    }

    void PlayerHealedEvent(PlayerBase attacker, int health, PlayerBase player)
    {

    }

    void PlayerKilledEvent(EnemyBase attacker, PlayerBase player)
    {

    }
    #endregion

    #region Spells
    void Element1AttunedEvent(spellElements element, PlayerBase player)
    {

    }

    void Element2AttunedEvent(spellElements element, PlayerBase player)
    {

    }

    void Element3AttunedEvent(spellElements element, PlayerBase player)
    {

    }

    void Element4AttunedEvent(spellElements element, PlayerBase player)
    {

    }

    void SpellInvokedEvent(spells spell, PlayerBase player)
    {

    }

    void SpellCastEvent(spells spell, PlayerBase player)
    {

    }
    #endregion

    #region Weapons
    void WeaponEquippedEvent(Weapons weapon, PlayerBase player)
    {

    }
    void WeaponUnequippedEvent(Weapons weapon, PlayerBase player)
    {

    }
    #endregion

    #endregion
}
