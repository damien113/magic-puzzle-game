﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{    /// <summary>
     /// Ditcionary to hold all the values for each stat.
     /// </summary>
    public Dictionary<Stats, int> stats = new Dictionary<Stats, int>();

    /// <summary>
    /// The amount of points that the player has for stats, including the ones being used, and thes ones not.
    /// </summary>
    public int totalPoints;

    /// <summary>
    /// The amount of unused points the player has to use for stats.
    /// </summary>
    public int unusedPoints;

    /// <summary>
    /// The amount of points being used on the players stats.
    /// </summary>
    public int usedPoints;

    private void Start()
    {
        ResetStats();
        SetupCommands();
    }


    #region Debug Console Commands
    /// <summary>
    /// Sets up all the commands so they work properly for debugging.
    /// </summary>
    void SetupCommands()
    {
        GConsole.AddCommand("add_points", "<color=green>Format is - SKILL.AMOUNT</color> Allows you to manually add points to a specific stat.", AddPointsCommand);
        GConsole.AddCommand("remove_points", "<color=green>Format is - SKILL.AMOUNT</color> Allows you to manually remove points from a specific stat.", RemovePointsCommand);
        GConsole.AddCommand("set_points", "<color=green>Format is - SKILL.AMOUNT</color> Allows you to manually set a players stat.", SetPointsCommand);
        GConsole.AddCommand("reset_points", "Allows you to manually reset all of the players stats.", ResetPointsCommand);
        GConsole.AddCommand("get_stat", "Gets a specified stats current level.", GetStatCommand);
        GConsole.AddCommand("list_stats", "Lists all the stats and thier levels.", ListStatsCommand);
    }

    public string GetStatCommand(string param)
    {
        Stats stat = (Stats)Enum.Parse(typeof(Stats), param);
        if (stat != null)
        {
            return "<color=green> "+stat+" : "+stats[stat]+"</color>";
        }
        else
        {
            return "<color=red>Invalid skill name specified. To get a list of stats do list_stats command.</color>";
        }
    }

    public string ListStatsCommand(string param)
    {
        GConsole.Print("<color=yellow>*************************************************************************</color>");
        for (int i = 0; i < (int)Stats.COUNT; i++)
        {
            GConsole.Print("   " + (Stats)i + " - " + stats[(Stats)i]);
        }
        GConsole.Print("<color=yellow>*************************************************************************</color>");
        return "<color=orange>All stats and thier levels have been printed.</color>";
    }

    /// <summary>
    /// Handles the logic for the command to add from to a specific stat.
    /// </summary>
    /// <param name="param"></param>
    /// <param name="param2"></param>
    /// <returns></returns>
    public string AddPointsCommand(string param)
    {
        string[] parameters = param.Split(char.Parse("."));
        int amount = int.Parse(parameters[1]);
        Stats stat = (Stats)Enum.Parse(typeof(Stats), parameters[0]);
        if (amount < 1)
        {
            return "<color=red> Invalid amount of points provided to the command.</color>";
        }
        if (stat != null)
        {
            AddPoints(stat, amount);
            return "<color=green> " + amount + " points were added to " + stat + " by command</color>";
        }
        return "<color=red>Invalid error occured.</color>";
    }

    /// <summary>
    /// Handles the logic for the command to remove points from a stat.
    /// </summary>
    /// <param name="param"></param>
    /// <param name="param2"></param>
    /// <returns></returns>
    public string RemovePointsCommand(string param)
    {
        string[] parameters = param.Split(char.Parse("."));
        int amount = int.Parse(parameters[1]);
        Stats stat = (Stats)Enum.Parse(typeof(Stats), parameters[0]);
        if (amount < 1)
        {
            return "<color=red> Invalid amount of points provided to the command.</color>";
        }
        if (stat != null)
        {
            RemovePoints(stat, amount);
            return "<color=green> " + amount + " points were remove from " + stat + " by command</color>";
        }
        return "<color=red>Invalid error occured.</color>";
    }

    /// <summary>
    /// Handles the logic for the command to set a specified stats level.
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public string SetPointsCommand(string param)
    {
        string[] parameters = param.Split(char.Parse("."));
        int amount = int.Parse(parameters[1]);
        Stats stat = (Stats)Enum.Parse(typeof(Stats), parameters[0]);
        if (amount < 1)
        {
            return "<color=red> Invalid amount of points provided to the command.</color>";
        }
        if (stat != null)
        {
            SetPoints(stat, amount);
            return "<color=green> " + amount + " points were set as the amount of points for " + stat + " by command</color>";
        }
        return "<color=red>Invalid error occured.</color>";
    }

    /// <summary>
    /// Resets every stats points/level to 1.
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public string ResetPointsCommand(string param)
    {
        ResetStats();
        return "<color=orange>All skills have had all thier points reset back to 1.</color>";
    }
    #endregion



    /// <summary>
    /// Lets you set the value of a stat manually. Probally most useful for debugging and balancing.
    /// </summary>
    /// <param name="The stat to set."></param>
    /// <param name="The value to set the stat as."></param>
    public bool SetPoints(Stats Stat, int Value)
    {
        if (Value >= 1)
        {
            stats[Stat] = Value;
            GConsole.Print("<color=green>Stat : '" + Stat + "' was set to " + Value + "</color>");
            //Event Broadcasts
            Messenger.Broadcast<Stats, int, PlayerBase>("stat_updated", Stat, Value, GetComponent<PlayerBase>());
            Messenger.Broadcast<Stats, int, PlayerBase>("stat_set", Stat, Value, GetComponent<PlayerBase>());
            return true;
        }
        else
        {
            GConsole.Print("<color=red>Attempted to set '" + Stat + "' as a invalid value. (Less than 1)</color>");
            return false;
        }

    }

    /// <summary>
    /// This function is used to add points from a stat. Unless the amount variable is passed, then it will only add one point.
    /// </summary>
    /// <param name="The stat to add to"></param>
    /// <param name="The amount to add"></param>
    /// <returns></returns>
    public bool AddPoints(Stats Stat, int Amount)
    {
        if (Amount >= 1)
        {
            stats[Stat] = stats[Stat] + Amount;
            GConsole.Print("<color=green>Added " + Amount + " points to '" + Stat + "'</color>");
            //Event Broadcasts
            Messenger.Broadcast<Stats, int, PlayerBase>("stat_updated", Stat, Amount, GetComponent<PlayerBase>());
            Messenger.Broadcast<Stats, int, PlayerBase>("stat_addedpoints", Stat, Amount, GetComponent<PlayerBase>());
            return true;
        }
        else
        {
            GConsole.Print(" < color = red > Attempted to add a amount of points to '" + Stat + "' That was invalid. (Less than 1) If you are trying to remove use the RemovePoints function.</color>");
            return false;
        }
    }

    /// <summary>
    /// This function is used to remove points from a stat. Unless the amount variable is passed, then it will only remove one point.
    /// </summary>
    /// <param name="The stat to remove from"></param>
    /// <param name="The amount to remove"></param>
    /// <returns></returns>
    public bool RemovePoints(Stats Stat, int Amount)
    {
        // Check that the stat has more than one point before trying to remove it
        if (Amount <= -1 && stats[Stat]>1)
        {
            stats[Stat] = stats[Stat] + Amount;
            GConsole.Print("<color=green>Added " + Amount + " points to '" + Stat + "'");
            //Event Broadcasts
            Messenger.Broadcast<Stats, int, PlayerBase>("stat_updated", Stat, Amount,GetComponent<PlayerBase>());
            Messenger.Broadcast<Stats, int, PlayerBase>("stat_removedpoints", Stat, Amount, GetComponent<PlayerBase>());
            return true;
        }
        else
        {
            GConsole.Print(" color = red > Attempted to add a amount of points to '" + Stat + "' That was invalid. (Less than 1) If you are trying to remove use the RemovePoints function. Or the skill only has 1 point already.</color>");
            return false;
        }
    }

    /// <summary>
    /// This function is to set all the stats back to what they are in the beggining. This is also used as a way of initially setting the stats to thier defualts in the beggining.
    /// </summary>
    public void ResetStats()
    {
        Messenger.Broadcast<PlayerBase >("stats_reset",GetComponent<PlayerBase>());
        stats[Stats.STRENGTH]=1;
        stats[Stats.INTELLIGENCE]=1;
        stats[Stats.VITALITY]=1;
        stats[Stats.WISDOM]=1;
    }

}
