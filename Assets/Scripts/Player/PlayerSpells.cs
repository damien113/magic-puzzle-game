﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerSpells : MonoBehaviour {
    /// <summary>
    /// The current element slot that is being set.
    /// </summary>
    int _currentElementSlot = 0;

    /// <summary>
    /// The current diction for the spell in the spelldictionarys variable.
    /// </summary>
    spellDictionary _currentSpellDictionary;

    /// <summary>
    /// A array to keep track of what element slot has what element attunted.
    /// </summary>
    spellElements[] _elementSlots = { spellElements.NONE, spellElements.NONE, spellElements.NONE, spellElements.NONE };

    /// <summary>
    /// A list of serialized class that holds all the information needed for each spell.
    /// </summary>
    public List<spellDictionary> spellDictionarys = new List<spellDictionary>();

    private void Start()
    {
        ResetElementSlots();
        SetupDebugCommands();
    }

    #region Debug Commandsz
    /// <summary>
    /// This sets up the commands with the GConsole framewoork so that the commands work properly.
    /// </summary>
    void SetupDebugCommands()
    {
        GConsole.AddCommand("set_spell", "Allows you to manually set the current spell for your player.",SetSpellCommand);
        GConsole.AddCommand("cast_spell", "Allows you to manually cast a specified spell. It quickly invokes,casts, then un evokes a spell. Any previously invoked spell would be reset.", CastSpellCommand);
        GConsole.AddCommand("list_spells", "List all the spells and all thier information.", ListSpellInformation);
    }

    /// <summary>
    /// This sets the currently invoked spell manually.
    /// </summary>
    /// <param name="param">Paremeter for the command.</param>
    /// <returns></returns>
    string SetSpellCommand(string param)
    {

        if(param.ToLower() == "clear" || param.ToLower() == "null" || param.ToLower() == "none")
        {
            return "<color=yellow>Cleared the current spell, nothing is invoked any longer.</color>";
        }
        foreach(spellDictionary diction in spellDictionarys)
        {
            if (diction.spell.ToString().ToLower() == param.ToLower())
                {
                _currentSpellDictionary = diction;
                return "<color=green>The spell '" + diction.spellName + "/" + diction.spell + "' was manually invoked and set</color>";
            }
        }
        return "<color=red>Invalid spell parameter passed.</color>";
    }

    /// <summary>
    /// Function for command to casta spell. It invoked the spell, casts, then un invokes it.
    /// </summary>
    /// <param name="param"> Paremeter for the command</param>
    /// <returns></returns>
    public string CastSpellCommand(string param)
    {
        foreach (spellDictionary diction in spellDictionarys)
        {
            if (diction.spell.ToString().ToLower() == param.ToLower())
            {
                _currentSpellDictionary = diction;
                CastSpell();
                _currentSpellDictionary = null;
                return "<color=green>The spell '" + diction.spellName + "/" + diction.spell + "' was manually cast.</color>";
            }
        }
        return "<color=red>Invalid spell parameter passed.<color>";
    }

    public string ListSpellInformation(string param)
    {
        GConsole.Print("**Spell Name*********Enum**********************Prefab******************************");
        foreach (spellDictionary diction in spellDictionarys)
        {
            GConsole.Print("|   "+diction.spellName+ "   |   " + diction.spell.ToString()+ "   |   " + diction.prefab + "|");
        }
        GConsole.Print("*********************************************************************************************");
        return "All the spells and thier information has been printed to the console.";
    }
    #endregion
    /// <summary>
    /// Called when a element button is press. It goes through the 3 slots, and if they are all taken , then it resets them all.
    /// </summary>
    /// <param name="Element To Set The Button  As."></param>
    public void SetElementSlot(spellElements element)
    {
        switch (_currentElementSlot)
        {
            case 0:
                _elementSlots[0] = element;
                _currentElementSlot++;
                GConsole.Print("<color=orange> Element 1 slot attuned to '" + _elementSlots[0] + "'</color>");
                Messenger.Broadcast<spellElements, PlayerBase>("spells_element1_attuned", element, GetComponent<PlayerBase>());
                break;
            case 1:
                _elementSlots[1] = element;
                _currentElementSlot++;
                GConsole.Print("<color=orange> Element 2 slot attuned to '" + _elementSlots[1] + "'</color>");
                Messenger.Broadcast<spellElements, PlayerBase>("spells_element2_attuned", element, GetComponent<PlayerBase>());
                break;
            case 2:
                _elementSlots[2] = element;
                _currentElementSlot++;
                GConsole.Print("<color=orange> Element 3 slot attuned to '" + _elementSlots[2] + "'</color>");
                Messenger.Broadcast<spellElements, PlayerBase>("spells_element3_attuned", element, GetComponent<PlayerBase>());
                break;
            case 3:
                _elementSlots[3] = element;
                _currentElementSlot++;
                GConsole.Print("<color=orange> Element 4 slot attuned to '" + _elementSlots[3] + "'</color>");
                Messenger.Broadcast<spellElements, PlayerBase>("spells_element4_attuned", element, GetComponent<PlayerBase>());
                break;
            case 4:
                ResetElementSlots();
                break;
        }
    }

    /// <summary>
    /// This resets all the element slots to none, and then sets the current slot to 1 so the player can choose a new combination of  elements.
    /// </summary>
    public void ResetElementSlots()
    {
        _currentElementSlot = 0;
        _elementSlots[0] = spellElements.NONE;
        _elementSlots[1] = spellElements.NONE;
        _elementSlots[2] = spellElements.NONE;
        _elementSlots[3] = spellElements.NONE;
        GConsole.Print("<color=orange>All element slots have been un-attuned.</color>");
    }

    /// <summary>
    /// Checks the all combinations tosee if any match the combination of attuned elements, and if so then set the current spell prefab to that attuned spells prefab.
    /// </summary>
    public void InvokeSpell()
    {
        if (_elementSlots.Contains(spellElements.NONE))
        {
            GConsole.Print("<color=red>Not all element slots are attuned.</color>");
            if (_currentSpellDictionary.prefab != null)
            {
                GConsole.Print("<color=orange>Any invoked spell has been cleared</color>");
                _currentSpellDictionary = null;
            }
        }
        else
        {
            int index = 0;
            foreach (spellDictionary diction in spellDictionarys)
            {
                if (diction.elements.SequenceEqual(_elementSlots))
                {
                    GConsole.Print("<color=green>The spell '"+diction.spell+"' was invoked!</color>");
                    _currentSpellDictionary = diction;
                    GetComponent<Animator>().SetFloat("SpellType",(float)(int)diction.animation);
                    ResetElementSlots();
                    Messenger.Broadcast<spells, PlayerBase>("spells_invoked", _currentSpellDictionary.spell, GetComponent<PlayerBase>());
                    return;
                }
                else
                {
                    GConsole.Print("<color=red> Invalid combination of elements</color>");
                }
                index++;
            }
            _currentSpellDictionary.prefab = null;
        }
    }

    /// <summary>
    /// This functional initalizes and casts the current spell.
    /// </summary>
    public void CastSpell()
    {
        if (_currentSpellDictionary.prefab != null && !GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).IsName("Cast Spell"))
        {
            GConsole.Print("<color=green>"+ _currentSpellDictionary.spellName+ "("+ _currentSpellDictionary.spell+ ") has been cast.</color>");
            GetComponent<Animator>().SetTrigger("CastSpell");
            Messenger.Broadcast<spells, PlayerBase>("spells_cast", _currentSpellDictionary.spell, GetComponent<PlayerBase>());
            GetComponent<PlayerCamera>().ShakeCamera(0.5f);
        }
        else
        {
            GConsole.Print("<color=red> Spell not yet invoked. Invoke a spell, check that the spell's prefab is set.</color>");
        }
    }

    public void SpellLogic()
    {
        GameObject tmp = Instantiate(_currentSpellDictionary.prefab);
        tmp.transform.eulerAngles = transform.eulerAngles;
        tmp.transform.position = transform.position+transform.forward;
        if (_currentSpellDictionary.type == SpellTypes.PROJECTILE)
        {
            tmp.GetComponent<BaseProjectile>().owner = gameObject;
        }

    }
}
