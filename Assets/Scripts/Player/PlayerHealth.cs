﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour {
    /// <summary>
    /// The maximum amount of health the player can have.
    /// </summary>
    public int maxHitPoints;
    /// <summary>
    /// The amount of health the player currently has.
    /// </summary>
    public int hitPoints;

    private void Start()
    {
        SetupCommands();
    }

    #region Debug Commands

    /// <summary>
    /// Sets up all the commands that are used for debugging and are relevant to this class.
    /// </summary>
    public void SetupCommands()
    {
        GConsole.AddCommand("debug_damage", "Allows you to manually damage the player for a specified amount. Used for debugging.", DebugDamageCommand);
        GConsole.AddCommand("debug_heal", "Allows you t o manually heal the player for a specified amount, used for debugging.", DebugHealCommand);
        GConsole.AddCommand("kill", "Allows you to manually kill the player. Used for debug purposes.", KillCommand);
        GConsole.AddCommand("set_health", "Allows you to manually set the player health..", SetHealthCommand);
        GConsole.AddCommand("set_max_health", "Allows you to manually set the player health..", SetMaxHealthCommand);
    }

    /// <summary>
    /// This handles the damage command for debugging, all it does is check that the integer is valid
    /// </summary>
    /// <param name="param"></param>
    /// <returns></returns>
    public string DebugDamageCommand(string param)
    {
        int paramint;
        if (int.TryParse(param, out paramint))
        {
            Damage(null, paramint);
            return "<color=orange>Player was dealt " + paramint + " damage</color>";
        }
        else
        {
            return "<color=red>Invalid amount passed for damage.</color>";
        }
    }

    /// <summary>
    /// This handles the heal command, it makes sure the parameter passed is a integer.
    /// </summary>
    /// <param name="param">paremeter for command</param>
    /// <returns></returns>
    public string DebugHealCommand(string param)
    {
        int paramint;
        if (int.TryParse(param, out paramint))
        {
            Heal(null, paramint);
            return "<color=green>Player was healed " + paramint + ".</color>";
        }
        else
        {
            return "<color=red>Invalid amount passed for heal</color>";
        }
    }

    /// <summary>
    /// This is a function to handle the kill command.
    /// </summary>
    /// <param name="param">paremeter for command</param>
    /// <returns></returns>
    public string KillCommand(string param)
    {
        Kill(null);
        return "Player has been killed by the 'kill' command.";
    }

    /// <summary>
    /// Function to handle the command that lets you manually set the health of player.
    /// </summary>
    /// <param name="param">paremeter for command</param>
    /// <returns></returns>
    public string SetHealthCommand(string param)
    {
        int paramint;
        if (int.TryParse(param, out paramint))
        {
            SetCurrentHealth(paramint);
            return "<color=green>Players health set by command.</color>";
        }
        else
        {
            return "<color=red>Invalid amount passed for damage.</color>";
        }
    }

    /// <summary>
    /// Function to handle the command that lets you manually set the maximum health of player.
    /// </summary>
    /// <param name="param">paremeter for command</param>
    /// <returns></returns>
    public string SetMaxHealthCommand(string param)
    {
        int paramint;
        if (int.TryParse(param, out paramint))
        {
            SetMaxHealth(paramint,true);
            return "<color=green>Players maximum health set by command.</color>";
        }
        else
        {
            return "<color=red>Invalid amount passed for damage.</color>";
        }
    }
    #endregion





    /// <summary>
    /// This function damages the player. It triggers a event "ply damaged_event"
    /// </summary>
    /// <param name="The gameobject of what attacked the player."></param>
    /// <param name="The amount of damage to apply."></param>
    public void Damage(EnemyBase attacker, int damage)
    {
        int tmp = hitPoints - damage;
        if (tmp > 0)
        {
            hitPoints = tmp;
            Messenger.Broadcast<EnemyBase, int,PlayerBase>("player_damaged",attacker,damage,GetComponent<PlayerBase>());
        }
        else
        {
            Kill(attacker);
        }
    }

    /// <summary>
    /// This function heals the player. It triggers a event "ply healed_event"
    /// </summary>
    /// <param name="The gameobject of what healed the player."></param>
    /// <param name="The amount of health to apply."></param>
    public void Heal(PlayerBase healer, int health)
    {
        int tmp = hitPoints + health;
        if (tmp < maxHitPoints)
        {
            hitPoints = tmp;
            Messenger.Broadcast<PlayerBase, int, PlayerBase>("player_healed", healer, health, GetComponent<PlayerBase>());
        }
        else
        {
            hitPoints = maxHitPoints;
        }
    }

    /// <summary>
    /// This is a functiont that kills the player. It broadcasts am essage that theplayer died called "ply_death_event"
    /// </summary>
    /// <param name="The gameobject who killed the player."></param>
    public void Kill(EnemyBase killer)
    {
        Destroy(gameObject);
        Messenger.Broadcast<EnemyBase, PlayerBase>("player_killed", killer, GetComponent<PlayerBase>());
    }

    /// <summary>
    /// This will set the health of the player to a value that is passed as a argument. This was added so it could be called when the
    /// vitality stat was upgraded on the players stats. A event  called "ply_maxhpset_event" is broadcasted at the end.
    /// </summary>
    /// <param name="The amountof health to set."></param>
    /// <param name="Should the current health is to be set?"></param>
    public void SetMaxHealth(int health, bool setCurrentHealth)
    {
        if (setCurrentHealth)
        {
            maxHitPoints = health;
            hitPoints = maxHitPoints;
        }
        else
        {
            maxHitPoints = health;
        }
    }

    /// <summary>
    /// This function allows you to manually set the current health of the player.
    /// </summary>
    /// <param name="healthToSet">Amount to set the health as</param>
    public void SetCurrentHealth(int healthToSet)
    {
        if (healthToSet >= 1)
        {
            hitPoints = healthToSet;
            Debug.Log("<color=orange>Players health has been manually set to '"+healthToSet+"'.</color>");
        }
        else
        {
            Debug.Log("<color=red>The number passed to SetCurrentHealth was below 1. It needs to be 1 or higher to set the health as that number.</color>");
        }
    }

}
