﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public float Dampening = 0.3f;
    public float xOffset = 0;
    public float zOffset = 0;
    public float minZoom = 8;
    public float maxZoom = 12;
    public float curZoom = 10;
    public float zoomRate = 1;
    private Vector3 velocity = Vector3.zero;

    public float shakeDuration = 0f;
    public float shakeAmount = 0.7f;
    public float decreaseFactor = 1.0f;

    Vector3 originalPos;
    
    void Awake()
    {
    }

    public void ShakeCamera(float seconds)
    {
        originalPos = Camera.main.transform.localPosition;
        StartCoroutine(Shake(DateTime.Now.AddSeconds(seconds)));
    }

    IEnumerator Shake(DateTime time)
    {
        while(DateTime.Now < time)
        {

            Camera.main.transform.localPosition = originalPos + UnityEngine.Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
            

            yield return new WaitForSeconds(0.1f);
        }
        Camera.main.transform.localPosition = originalPos;
    }
    void LateUpdate()
    {
        Vector3 _targetPosition = transform.position;
        _targetPosition.x += xOffset;
        _targetPosition.z += zOffset;
        _targetPosition.y = transform.position.y+ curZoom;
        Camera.main.transform.position = Vector3.SmoothDamp(Camera.main.transform.position, _targetPosition, ref velocity, Dampening);
    }


}