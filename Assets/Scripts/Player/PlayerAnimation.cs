﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour {

    public Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        float HMovementAxis = Input.GetAxis("Horizontal Movement");
        float VMovementAxis = Input.GetAxis("Vertical Movement");
        float HDirectionAxis = Input.GetAxis("Horizontal Direction");
        float VDirectionAxis = Input.GetAxis("Vertical Direction");

        anim.SetFloat("BlendX", HMovementAxis);
        anim.SetFloat("BlendY", VMovementAxis);

    }
}
