﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerControls : MonoBehaviour {
    public float dashDistance = 10.0f;
    Directions _lastKnownDirection;
    Dictionary<Directions, Quaternion> _directionRotations = new Dictionary<Directions, Quaternion>();
    NavMeshAgent _navAgent;
    bool isOpen = false;
    bool canCast = true;
    public GameObject console;
    private void Start()
    {
        _navAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {

        if (Input.GetKeyDown("`"))
        {
            if (isOpen)
            {
                isOpen = false;
                console.SetActive(false);
            }
            else
            {
                isOpen = true;
                console.SetActive(true);
            }
        }
    }
    private void FixedUpdate()
    {
        float HMovementAxis = Input.GetAxis("Horizontal Movement");
        float VMovementAxis = Input.GetAxis("Vertical Movement");
        float HDirectionAxis = Input.GetAxis("Horizontal Direction");
        float VDirectionAxis = Input.GetAxis("Vertical Direction");
        float CameraZoom = Input.GetAxis("Camera Zoom");
        if (CameraZoom != 0)
        {
            if (GetComponent<PlayerCamera>().maxZoom > GetComponent<PlayerCamera>().curZoom)
            {
                float newZoom = GetComponent<PlayerCamera>().curZoom + GetComponent<PlayerCamera>().zoomRate * CameraZoom * Time.deltaTime;
                if (newZoom > GetComponent<PlayerCamera>().maxZoom)
                {
                    GetComponent<PlayerCamera>().curZoom = GetComponent<PlayerCamera>().maxZoom;
                }
                else
                {
                    GetComponent<PlayerCamera>().curZoom = newZoom;
                }
            }

            if (GetComponent<PlayerCamera>().minZoom < GetComponent<PlayerCamera>().curZoom)
            {
                float newZoom = GetComponent<PlayerCamera>().curZoom + GetComponent<PlayerCamera>().zoomRate*CameraZoom * Time.deltaTime;
                if (newZoom < GetComponent<PlayerCamera>().minZoom)
                {
                    GetComponent<PlayerCamera>().curZoom = GetComponent<PlayerCamera>().minZoom;
                }
                else
                {
                    GetComponent<PlayerCamera>().curZoom = newZoom;
                }
            }
        }

        // REPLACED WITH ROOT MOTION SYSTEM IGNORE THIS.
        // Move the player based on the horizontal and vertical input axis.
        //if (HMovementAxis != 0 || VMovementAxis != 0)
        //{
        //    _navAgent.Move(new Vector3(HMovementAxis * movementSpeed * Time.deltaTime, 0, VMovementAxis * movementSpeed * Time.deltaTime));
        //}

        //Rotate the player according to the right joystick on the controller


        if (HDirectionAxis != 0 || VDirectionAxis != 0)
        {
            float angle = Mathf.Atan2(VDirectionAxis, HDirectionAxis) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis((90-angle)*-1, Vector3.up);
        }

        if (Input.GetAxis("Cast Trigger")<-0.75f && canCast)
        {
            GetComponent<PlayerSpells>().CastSpell();
            canCast = false;
            StartCoroutine(resetCanCast());
        }
        // Checks for attune modifier.
        if (Input.GetButton("Attune"))
        {
            if (Input.GetButtonDown("Air"))
            {
                GetComponent<PlayerSpells>().SetElementSlot(spellElements.AIR);
            }
            if (Input.GetButtonDown("Earth"))
            {
                GetComponent<PlayerSpells>().SetElementSlot(spellElements.EARTH);
            }
            if (Input.GetButtonDown("Fire"))
            {
                GetComponent<PlayerSpells>().SetElementSlot(spellElements.FIRE);
            }
            if (Input.GetButtonDown("Water"))
            {
                GetComponent<PlayerSpells>().SetElementSlot(spellElements.WATER);
            }
        }
        else
        {
            if (Input.GetButtonDown("Dash"))
            {
                if (HMovementAxis != 0 || VMovementAxis != 0)
                {
                    _navAgent.Warp(transform.position +new Vector3(HMovementAxis * dashDistance, 0, VMovementAxis * dashDistance));
                }
                else
                {
                    _navAgent.Warp(transform.position + (transform.forward * dashDistance));
                }
                        
            }
            if (Input.GetButtonDown("Melee"))
            {
                GetComponent<PlayerMelee>().MeleeAttack();
            }
            if (Input.GetButtonDown("Invoke"))
            {
                GetComponent<PlayerSpells>().InvokeSpell();
            }
            if (Input.GetButtonDown("Cast"))
            {
                GetComponent<PlayerSpells>().CastSpell();
            }
        }
    }

    IEnumerator resetCanCast()
    {
        yield return new WaitForSeconds(0.35f);
        canCast = true;
    }
}
