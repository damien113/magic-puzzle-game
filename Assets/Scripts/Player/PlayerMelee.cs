﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMelee : MonoBehaviour
{
    /// <summary>
    /// The collider where if enemys are in it, they take damage when the player does melee attacks.
    /// </summary>
    public BoxCollider meleeHitbox;

    /// <summary>
    /// The cooldown between attacks.
    /// </summary>
    public float attackSpeed;

    private void Start()
    {
        Messenger.AddListener<Stats, int, PlayerBase>("stat_updated", StatUpdatedEvent);
    }

    /// <summary>
    /// Handles the logic for the event handler for the event stat_updated, so that when any stats that effect combat are updated that they can adjust the current damage and etc of the player to match the stat.
    /// </summary>
    /// <param name="stat"></param>
    /// <param name="points"></param>
    /// <param name="player"></param>
    void StatUpdatedEvent(Stats stat, int points, PlayerBase player)
    {
        // using a switch statement in case that we ever have another stat that would effect melee damage.
        switch (stat)
        {
            case Stats.STRENGTH:

                break;
        }
    }


    public void MeleeAttack()
    {
        Collider[] _collidesInHitbox = Physics.OverlapSphere(meleeHitbox.transform.position, meleeHitbox.size.x / 2);
        foreach (Collider target in _collidesInHitbox)
        {
            if (target.GetComponent<EnemyBase>() != null)
            {
                target.GetComponent<EnemyHealth>().Damage(GetComponent<PlayerBase>(), GetComponent<PlayerWeapons>()._currentWeapon.weaponDamage + GetComponent<PlayerStats>().stats[Stats.STRENGTH]);
            }
        }
    }
}

