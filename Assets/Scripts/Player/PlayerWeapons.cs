﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapons : MonoBehaviour {
    /// <summary>
    /// The current diction for the weapon that is equiped.
    /// </summary>
    [HideInInspector]
    public weaponDiction _currentWeapon;
    
    /// <summary>
    /// The current gameobject that holds the script to provid special logic for the weapon.
    /// </summary>
    [HideInInspector]
    public GameObject _currentWeaponObject;
    public List<weaponDiction> weapons = new List<weaponDiction>();
    
    /// <summary>
    /// Equip a specified weapon using its enum.
    /// </summary>
    /// <param name="weapon">The enum of the weapon</param>
    public void EquipByEnum(Weapons weapon)
    {
        foreach(weaponDiction diction in weapons)
        {
            if (diction.weaponEnum == weapon)
            {
                _currentWeaponObject.GetComponent<BaseWeapon>().UnEquip();
                Destroy(_currentWeaponObject);
                Messenger.Broadcast<Weapons, PlayerBase>("weapon_unequipped", _currentWeapon.weaponEnum, GetComponent<PlayerBase>());
                _currentWeapon = diction;
                GameObject tmp = Instantiate(_currentWeapon.weaponPrefab);
                tmp.transform.parent = transform;
                _currentWeaponObject.GetComponent<BaseWeapon>().Equip();
                Messenger.Broadcast<Weapons, PlayerBase>("weapon_equipped", _currentWeapon.weaponEnum, GetComponent<PlayerBase>());
            }
        }
    }

    /// <summary>
    /// Equip a specified weapon using its name.
    /// </summary>
    /// <param name="weapon">The name of the weapon</param>
    public void EquipByName(string weapon)
    {
        foreach (weaponDiction diction in weapons)
        {
            if (diction.weaponName == weapon)
            {
                _currentWeaponObject.GetComponent<BaseWeapon>().UnEquip();
                Destroy(_currentWeaponObject);
                Messenger.Broadcast<Weapons, PlayerBase>("weapon_unequipped", _currentWeapon.weaponEnum, GetComponent<PlayerBase>());
                _currentWeapon = diction;
                GameObject tmp = Instantiate(_currentWeapon.weaponPrefab);
                tmp.transform.parent = transform;
                _currentWeaponObject.GetComponent<BaseWeapon>().Equip();
                Messenger.Broadcast<Weapons, PlayerBase>("weapon_equipped", _currentWeapon.weaponEnum, GetComponent<PlayerBase>());
            }
        }
    }
}
